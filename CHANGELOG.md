2.1.0
------

* Added support for SkyLakeAVX512, CascadeLake and POWER9 processors ([MR19](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon/-/merge_requests/19))
* Use `archspec` to detect micro architectures ([MR18](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon/-/merge_requests/18))


2.0.0
------

* Added new exporters for metric files ([MR13](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon/-/merge_requests/13))
* Added support for NVIDIA GPU metrics ([MR10](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon/-/merge_requests/10))


1.1.0
------

* Added new `prefix` CLI option to name the directory where metrics will be saved ([MR5](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon/-/merge_requests/5))
* Added function dispatching for finding PIDs and exporting metric data ([MR4](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon/-/merge_requests/4))


1.0.0
------

* Added unittests ([MR3](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon/-/merge_requests/3))
* Added API documentation ([MR2](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon/-/merge_requests/2))
* Refactored entire code base to make it more modular ([MR1](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon/-/merge_requests/1))
