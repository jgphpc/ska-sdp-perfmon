"""This module contains class for detecting process PIDs for various schedulers"""

import logging
import time

from perfmon.common.pid.pid import if_script_name_in_chilren
from perfmon.common.utils.process import find_procs_by_name


_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def register_pbs_oar(scheduler):
    """This method returns the pid of the OAR and PBS jobs"""
    # Typical spawning of processes on PBS looks as below. Here we assume sdpmonitormetrics is the
    # monitor job and hello is the main job
    # On head node:
    # bash(1)───pbs_mom(85)───bash(1069)─┬─13.pbs_master.S(1095)─┬─mpirun(1096)─┬─sdpmonitormetri(1102)───{sdpmonitormetri}(1103)
    #                                    │                       │              ├─ssh(1101)
    #                                    │                       │              └─{mpirun}(1099)
    #                                    │                       └─mpirun(1097)─┬─hello(1104)───{hello}(1110)
    #                                    │                                      ├─hello(1105)───{hello}(1112)
    #                                    │                                      ├─hello(1106)───{hello}(1115)
    #                                    │                                      ├─hello(1107)───{hello}(1118)
    #                                    │                                      ├─hello(1108)───{hello}(1114)
    #                                    │                                      ├─hello(1109)───{hello}(1117)
    #                                    │                                      ├─hello(1111)───{hello}(1119)
    #                                    │                                      ├─hello(1113)───{hello}(1116)
    #                                    │                                      ├─ssh(1100)
    #                                    │                                      └─{mpirun}(1098)
    #                                    └─pbs_demux(1070)
    # On slave nodes:
    # bash(1)─┬─pbs_mom(85)───{pbs_mom}(86)
    #         ├─sleep(93)
    #         └─sshd(163)─┬─sshd(497)───sshd(502)───orted(523)─┬─hello(543)───{hello}(552)
    #                     │                                    ├─hello(544)───{hello}(550)
    #                     │                                    ├─hello(545)───{hello}(558)
    #                     │                                    ├─hello(546)───{hello}(553)
    #                     │                                    ├─hello(547)───{hello}(554)
    #                     │                                    ├─hello(548)───{hello}(557)
    #                     │                                    ├─hello(549)───{hello}(555)
    #                     │                                    └─hello(551)───{hello}(556)
    #                     └─sshd(498)───sshd(501)───orted(503)───sdpmonitormetri(522)───{sdpmonitormetri}(528)
    # On head node we can use tracejob <jobid> command to get the pid of bash script, which in this case will yield
    # 1069. But tracejob command on slave nodes do not contain any PID information
    # So the best bet here is to look for process names. We see that mpirun is the one that spawns the processes
    # on the head node. We look for processes nameed mpirun and then check the process name of children
    # If the child doesnt contain sdpmonitormetrics (script name) that is the our main step job and we return PID
    # of that mpirun
    # In the case of slave nodes we spawning process looks quite complicated. We can looked for orted processes
    # and repeat the same process of checking for process names of children and filtering the main step job.

    # Job spawning process of OAR is very similar to PBS. Here is the schematic
    # Typical workflow of job launching in OAR on master node
    # systemd(1)───perl(7208)───perl(7209)───cpu-metrics-tes(7215)───mpirun(7350)─┬─matmul(7376)
    #                                                                             ├─ssh(7373)
    #                                                                             ├─{mpirun}(7369)
    #                                                                             ├─{mpirun}(7370)
    #                                                                             ├─{mpirun}(7371)
    #                                                                             └─{mpirun}(7372)
    # OAR as OAR user starts a perl process -> OAR starts perl process in user space -> Perl
    # process launches batch script -> Step jobs in batch script are launched
    # On rest of the nodes
    # systemd(1)───sshd(5103)───sshd(7381)───sshd(7387)───orted(7388)───matmul(7454)
    # So we should look for mpirun process on master node and orted process on slave nodes

    # In the case of Intel MPI there is an additional step between
    # mpiexec and jobs by hydra_pmi_proxy. So we check for that name

    _log.debug('Finding Job PID from OAR/PBS scheduler implementation')

    # On head node we should look for mpirun process
    proc_name = scheduler['launcher']
    if scheduler['launcher'] == 'mpirun':  # OpenMPI
        if scheduler['master_node'] == scheduler['node_name']:
            proc_name = 'mpirun'
        else:
            # On slaves we should look for orted
            proc_name = 'orted'
    elif scheduler['launcher'] == 'mpiexec':  # Intel MPI
        proc_name = 'hydra_pmi_proxy'

    start_time = time.time()
    num_try = 0
    while (time.time() - start_time) < scheduler['timeout'] or num_try < scheduler['max_retries']:
        # Get all processes with a proc_name
        mpirun_procs = find_procs_by_name(proc_name)
        _log.debug(
            'Try %d: Found processes of name %s is %s'
            % (num_try, proc_name, mpirun_procs)
        )
        found_pids = []
        for proc in mpirun_procs:
            job_pid = proc.pid
            child_procs = proc.children()
            child_proc_names = []
            # Get names of children processes
            for child_proc in child_procs:
                child_name = child_proc.name()
                if child_name not in ['ssh']:
                    child_proc_names.append(child_name)
            _log.debug(
                'Try %d: Children processes names are %s' % (num_try, child_proc_names)
            )
            # If children do not have script name that is main job
            if (child_proc_names
                    and not if_script_name_in_chilren(scheduler['script_names'], child_proc_names)):
                found_pids.append(job_pid)
            if found_pids:
                return found_pids
        time.sleep(1)
        num_try += 1

    # If pid is not found by either of these methods, fallback to naive method
    _log.warning(
        'Could not find %s job step PID on %s node. '
        'Falling back to naive method' % (scheduler['name'], scheduler['node_name'])
    )
