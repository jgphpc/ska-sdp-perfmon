"""This module contains class for detecting process PIDs for various schedulers"""

import logging
import time
import psutil

from perfmon.common.pid.pid import if_script_name_in_chilren
from perfmon.common.utils.process import get_proc_info
from perfmon.common.utils.execute_cmd import execute_cmd

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def get_slurm_mpirun_launcher_job_pid(step_id, scheduler):
    """This method gets the job pids launched with mpirun launcher"""

    # CASE -> LAUNCHED BY MPIRUN
    # if mpirun is used to launch the jobs, typical spawning process is as follows:
    # systemd(pid0)───slurmstepd(pid1)───slurm_script(pid2)─┬─mpirun(pid3)─┬─sdpmonitormetri(pidn1)─┬─sdpmonitormetri(pidn2)
    #                                                       │                                       └─sdpmonitormetri(pidn3)
    #                                                       │
    #                                                       └─mpirun(pid4)─┬─srun(pid5)─┬─srun
    #                                                                      │            ├─{srun}
    #                                                                      │            ├─{srun}
    #                                                                      │            └─{srun}
    #                                                                      ├─stream(pid6)─┬─{stream}(pidm1)
    #                                                                                     |    ....
    #                                                                                     └─{stream}(pidmk)
    #
    # Typical scontrol listpids <job_id> will yield
    # PID      JOBID    STEPID LOCALID GLOBALID
    # pid2    <id>     batch  0       0
    # pid3    <id>     batch  -       -
    # pid4    <id>     batch  -       -
    # So for the case of mpirun, it is more simpler as we need to capture only pid4 which is always spawned
    # after monitor job. So it will be the last one.
    # These details are for the head nodes in the reservation. For slave nodes, it will be same albeit
    # there will not be any batch jobs.

    start_time = time.time()
    while (time.time() - start_time < scheduler['timeout']) and step_id >= 0:
        job_id = scheduler['job_id']
        if scheduler['node_name'] == scheduler['master_node']:
            cmd_str = (
                f'scontrol listpids {job_id} | awk \'$3 == \"batch\" {{print $1}}\''
            )
        else:
            cmd_str = (
                f'scontrol listpids {job_id}.{step_id} | awk \'NR > 1 {{print $1}}\''
            )

        all_pids = execute_cmd(cmd_str).splitlines()
        _log.debug('Output of %s is %s' % (cmd_str, all_pids))
        for pid in all_pids:
            try:
                pid = int(pid)
                proc = get_proc_info(pid)
                if proc:
                    child_proc_names = [p.name() for p in proc.children(recursive=True)]
                    _log.debug(
                        'Child processes names are %s and parent '
                        'process name is %s' % (child_proc_names, proc.name())
                    )
                    # Sometimes the process name is truncated. we need to to check a string for substrings contained in a list.
                    if (proc.name() in ['mpirun', 'orted']
                        and proc.name() not in ['slurm_script', 'ssh']
                        and not if_script_name_in_chilren(
                            scheduler['script_names'], child_proc_names)):
                        return [pid]
            except ValueError:
                pass
        time.sleep(1)

    # If pid is not found by either of these methods, fallback to naive method
    _log.warning(
        'Could not find SLURM job step PID on %s node. '
        'Falling back to naive method' % scheduler['node_name']
    )


def get_slurm_srun_launcher_job_pid(step_id, scheduler):
    """This method gets the job pids launched with srun launcher"""

    # Finding Job PID in SLURM is tricky. It depends how the job is launched, ie, mpirun or srun.
    # We **strongly** recommend to launch both monitoring job and step job using same launcher
    #
    # CASE -> LAUNCHED BY SRUN
    # If srun is used to launch both step jobs, the spawning process is as follows:
    # For monitoring job it looks like this
    # systemd(pid0)───slurmstepd(pidn1)───sdpmonitormetri(pidn2)─┬─sdpmonitormetri(pidn3) ...
    #                                                            └─sdpmonitormetri(pidn4) ...
    # For main step job
    #
    # scontrol listpids <job_id> will yield a result as follows:
    # systemd(pid0)───slurmstepd(pidm1)─┬─stream(pidm2)─┬─{stream}
    #                                   │               |    ...
    #                                   │               └─{stream}
    #                                   |      .
    #                                   |      .
    #                                   |      .
    #                                   ├─stream(pidmk)─┬─{stream}
    #                                                   |    ...
    #                                                   └─{stream}
    # Typical scontrol listpids <job_id> will yield
    # PID      JOBID    STEPID LOCALID GLOBALID
    # pidm2    <id>     1      0       0
    #            ....
    # pidmk    <id>     1      k-1     k-1
    # pidb1    <id>     batch  0       0
    #            ....
    # pidbl    <id>     batch  -       -
    # pidn2    <id>     0      0       0
    #            ....
    # pidnn    <id>     0      -       -
    # Here pid0 is system PID which is 1, there are k main step jobs which are SLURM tasks. If we
    # sleep for couple of seconds after launching monitor job, we will ALWAYS have monitoring job
    # as step job 0. So, we check only for step job 1. All the step jobs of main job is spawned
    # by pidm1 and it is parent to all pids ranging from pidm2 to pidmk. So we check parents of
    # all step jobs of 1 and pass it as job PID.
    #

    start_time = time.time()
    while time.time() - start_time < scheduler['timeout'] and step_id >= 0:
        job_id = scheduler['job_id']
        cmd_str = f'scontrol listpids {job_id}.{step_id} | awk \'NR > 1 {{print $1}}\''
        all_pids = execute_cmd(cmd_str).splitlines()
        _log.debug('Output of %s is %s' % (cmd_str, all_pids))
        captured_pids = []
        captured_ppids = []
        child_proc_names = []
        for pid in all_pids:
            try:
                pid = int(pid)
                proc = get_proc_info(pid)
                if proc:
                    captured_pids.append(pid)
                    captured_ppids.append(proc.ppid())
                    child_proc_names.append(proc.name())
            except ValueError:
                pass
        # remove duplicates
        captured_ppids = list(set(captured_ppids))
        _log.debug(
            'Found job pids for step job %d are %s and parent is %s'
            % (
                step_id,
                ','.join([str(p) for p in captured_pids]),
                ','.join([str(p) for p in captured_ppids]),
            )
        )
        if len(captured_ppids) == 1:
            if (psutil.Process(captured_ppids[0]).name() == 'slurmstepd'
            ):
              #      and not if_script_name_in_chilren(scheduler['script_names'], child_proc_names)):
                return captured_pids
        time.sleep(1)

    # If pid is not found by either of these methods, fallback to naive method
    _log.warning(
        'Could not find SLURM job step PID on %s node. '
        'Falling back to naive method' % scheduler['node_name']
    )


def register_slurm(scheduler):
    """This method returns the pid of the SLURM step jobs"""

    _log.debug('Finding Job PID from SLURM scheduler implementation')

    for step_id in range(1, -1, -1):
        pids = None
        if scheduler['launcher'] in ['srun', 'mpiexec']:
            pids = get_slurm_srun_launcher_job_pid(step_id, scheduler)
        elif scheduler['launcher'] == 'mpirun':
            pids = get_slurm_mpirun_launcher_job_pid(step_id, scheduler)

        if pids is not None:
            return pids
