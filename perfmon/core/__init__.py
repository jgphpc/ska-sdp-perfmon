""""This package contains core modules of SDP performance monitoring toolkit"""

import os
import sys
import logging

from perfmon.core.parsers import parse_args
from perfmon.cfg import GlobalConfiguration
from perfmon.common.logging import logger_config
from perfmon.core.execution.wrapper import perfmon_entrypoint

# pylint: disable=E0401,W0201,C0301


def main(cmd_args=None):
    """Main function of SDP performance monitoring toolkit"""

    # parse args
    args = parse_args(cmd_args)

    # Convert arg parser namespace to dict
    config_maker = GlobalConfiguration(args)
    global_config = config_maker.create_config()

    # Create logger
    logger = logger_config(global_config)

    # Pre processing of monitoring
    perfmon_entrypoint(global_config)
