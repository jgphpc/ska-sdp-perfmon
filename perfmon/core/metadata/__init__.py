"""This file contains base class to extract meta data of the hardware"""

import os
import logging
import time
import sys
import socket
import shutil
import platform

from perfmon.common.utils.parsing import get_parser
from perfmon.common.utils.execute_cmd import execute_cmd
from perfmon.common.utils.json_wrappers import write_json

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class GetHWMetadata(object):
    """Engine to extract metadata"""

    def __init__(self, config):
        """Initialize setup"""

        self.config = config.copy()
        self._extra = {}
        self.data = {}
        self.outfile = os.path.join(
            self.config['temp_path']['meta_data'],
            '_'.join([str(self.config['job_id']), self.config['host_name']]) + '.json',
        )

    @staticmethod
    def collect_cpu_info():
        """Collect all relevant CPU information."""

        _log.info('Collecting CPU information.')

        # Get output from lscpu command
        cmd_out = execute_cmd('lscpu')

        # _log.debug('Output of lscpu command is %s', cmd_out)

        # Get parser function
        parse_lscpu = get_parser(cmd_out)

        def conv(func, typ):
            """Convert to a given type"""
            try:
                return typ(func)
            except ValueError:
                return func

        # Different meta data information returned by lspcu command
        cpu = {
            'Architecture': parse_lscpu('Architecture'),
            'CPU_Model': parse_lscpu('Model name'),
            'CPU_Family': parse_lscpu('CPU family'),
            'CPU_num': conv(parse_lscpu(r'CPU\(s\)'), int),
            'Online_CPUs_list': parse_lscpu(r'On-line CPU\(s\) list'),
            'Threads_per_core': conv(parse_lscpu(r'Thread\(s\) per core'), int),
            'Cores_per_socket': conv(parse_lscpu(r'Core\(s\) per socket'), int),
            'Sockets': conv(parse_lscpu(r'Socket\(s\)'), int),
            'Vendor_ID': parse_lscpu('Vendor ID'),
            'Stepping': conv(parse_lscpu('Stepping'), int),
            'CPU_MHz': conv(parse_lscpu('CPU MHz'), float),
            'CPU_Max_Speed_MHz': conv(parse_lscpu('CPU max MHz'), float),
            'CPU_Min_Speed_MHz': conv(parse_lscpu('CPU min MHz'), float),
            'BogoMIPS': conv(parse_lscpu('BogoMIPS'), float),
            'L1d_cache': parse_lscpu('L1d cache'),
            'L1i_cache': parse_lscpu('L1i cache'),
            'L2_cache': parse_lscpu('L2 cache'),
            'L3_cache': parse_lscpu('L3 cache'),
            'NUMA_nodes': conv(parse_lscpu(r'NUMA node\(s\)'), int),
        }

        # Populate NUMA nodes
        try:
            for i in range(0, int(cpu['NUMA_nodes'])):
                cpu['NUMA_node{}_CPUs'.format(i)] = parse_lscpu(r'NUMA node{} CPU\(s\)'.format(i))
        except ValueError:
            _log.warning('Failed to parse or NUMA nodes not existent.')

        # Update with additional data
        cpu.update(
            {
                'Power_Policy': execute_cmd(
                    'cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor ' '| sort | uniq'
                ),
                'Power_Driver': execute_cmd(
                    'cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_driver ' '| sort | uniq'
                ),
                'Microcode': execute_cmd(
                    'grep microcode /proc/cpuinfo | uniq | awk ' '\'NR==1{print $3}\''
                ),
                'SMT_Enabled?': bool(execute_cmd('cat /sys/devices/system/cpu/smt/active')),
            }
        )

        return cpu

    def collect_gpu_hmd(self):
        """Collect NVIDIA GPU hardware metadata"""

        _log.info('NVIDIA GPUs Found. Collecting GPU information.')

        # Import only when NVIDIA drivers are found. Else it may throw warnings
        import py3nvml

        try:
            py3nvml.py3nvml.nvmlInit()
        except py3nvml.py3nvml.NVMLError as err:
            _log.warning('Failed to initialise NVML: %s' % err)
            return {}

        gpu = {
            'Num_Devices': self.config['num_nvidia_gpus'],
            'Dev_Info': {},
        }

        for i in range(gpu['Num_Devices']):
            handle = py3nvml.py3nvml.nvmlDeviceGetHandleByIndex(i)
            gpu['Dev_Info'][f'gpu_{i}'] = {
                'Device_Name': py3nvml.py3nvml.nvmlDeviceGetName(handle),
                'Max_Clock_Info': {
                    'Graphics': py3nvml.py3nvml.nvmlDeviceGetMaxClockInfo(handle, 0),
                    'SM': py3nvml.py3nvml.nvmlDeviceGetMaxClockInfo(handle, 1),
                    'Mem': py3nvml.py3nvml.nvmlDeviceGetMaxClockInfo(handle, 2),
                },
                'Compute_Capability': py3nvml.py3nvml.nvmlDeviceGetCudaComputeCapability(handle),
                'BAR1_Mem_Total': int(
                    py3nvml.py3nvml.nvmlDeviceGetBAR1MemoryInfo(handle).bar1Total
                ),
                'Mem_Total': int(py3nvml.py3nvml.nvmlDeviceGetMemoryInfo(handle).total),
                'PCI_Info': {
                    'bus': py3nvml.py3nvml.nvmlDeviceGetPciInfo(handle).bus,
                    'busId': py3nvml.py3nvml.nvmlDeviceGetPciInfo(handle).busId.decode('utf-8'),
                    'device': py3nvml.py3nvml.nvmlDeviceGetPciInfo(handle).device,
                    'domain': py3nvml.py3nvml.nvmlDeviceGetPciInfo(handle).domain,
                    'pci_device_id': py3nvml.py3nvml.nvmlDeviceGetPciInfo(handle).pciDeviceId,
                    'pci_sub_system_id': py3nvml.py3nvml.nvmlDeviceGetPciInfo(
                        handle
                    ).pciSubSystemId,
                },
                'PCI_Link_Gen': py3nvml.py3nvml.nvmlDeviceGetCurrPcieLinkGeneration(handle),
                'PCI_Link_Width': py3nvml.py3nvml.nvmlDeviceGetCurrPcieLinkWidth(handle),
                'Power_Man_Mode': self.nvml_get_power_management_mode(py3nvml, handle),
                'Threshold_Temp': {
                    'Shutdown_Temp': self.nvml_get_temperature_threshold(py3nvml, handle, 0),
                    'Slowdown_Temp': self.nvml_get_temperature_threshold(py3nvml, handle, 1),
                    'Mem_Max_Temp': self.nvml_get_temperature_threshold(py3nvml, handle, 2),
                    'Gpu_Max_Temp': self.nvml_get_temperature_threshold(py3nvml, handle, 3),
                },
                'UUID': py3nvml.py3nvml.nvmlDeviceGetUUID(handle),
            }

        py3nvml.py3nvml.nvmlShutdown()

        return gpu

    @staticmethod
    def nvml_get_temperature_threshold(nvml, handle, num):
        try:
            return float(nvml.py3nvml.nvmlDeviceGetTemperatureThreshold(handle, num))
        except nvml.py3nvml.NVMLError_NotSupported:
            return None

    @staticmethod
    def nvml_get_power_management_mode(nvml, handle):
        try:
            return nvml.py3nvml.nvmlDeviceGetPowerManagementMode(handle)
        except nvml.py3nvml.NVMLError_NotSupported:
            return None

    @staticmethod
    def collect_memory_info():
        """Collect system memory."""

        _log.info('Collecting system memory.')

        # Get memory information from free command
        try:
            mem = {
                'Mem_Total': int(execute_cmd('free | awk \'NR==2{print $2}\'')),
                'Mem_Available': int(execute_cmd('free | awk \'NR==2{print $7}\'')),
                'Mem_Swap': int(execute_cmd('free | awk \'NR==3{print $2}\'')),
            }
        except ValueError:
            mem = {
                'Mem_Total': 0,
                'Mem_Available': 0,
                'Mem_Swap': 0,
            }

        return mem

    def collect_hmd(self):
        """Collect Hardware MetaData (HW_MD)."""

        _log.info('Collecting hardware specific information.')

        # caller functions to collect CPU and memory information
        self.data = {
            'cpu': self.collect_cpu_info(),
            'memory': self.collect_memory_info(),
        }

        # Collect GPU hardware metadata
        if self.config['num_nvidia_gpus'] > 0:
            self.data = {
                **self.data,
                'nv_gpu': self.collect_gpu_hmd(),
            }

    def collect(self):
        """Collect all metadata."""
        _log.info('Collecting the full metadata information.')

        self._extra['start_time'] = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())
        # Get name of the host
        self.data['host_name'] = socket.gethostname()

        # Collect hardware metadata
        self.collect_hmd()

        # Write the data to the disk in JSON format
        write_json(self.data, self.outfile)
        self._extra['end_time'] = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())
