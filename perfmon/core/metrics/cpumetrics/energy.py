"""Functions to monitor RAPL energy metrics"""

import logging

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def rapl_energy_readings(rapl_devices, data):
    """This method gets energy metrics from RAPL powercap interface"""

    # Get package/domain names and paths to find energy
    for dom_name, engy_path in rapl_devices.items():
        # cmd_str = ' '.join(['cat', engy_path])
        # cmd_out = execute_cmd(cmd_str)
        try:
            content = open(engy_path, 'r').read().rstrip()
            # Units are micro Joules. Convert them to Joules in post processing
            engy_value = int(content)
        except (PermissionError, ValueError):
            engy_value = 0
        # if dom_name not in data['rapl_powercap'].keys():
        #     self.cpu_data['rapl_powercap'][dom_name] = []
        data['rapl_powercap'][dom_name].append(engy_value)

    return data
