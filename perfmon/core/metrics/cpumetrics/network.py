"""Functions to monitor network related metrics"""

import os
import logging
import psutil

_log = logging.getLogger(__name__)

# Infiniband metrics API directory
IB_API_AIR = '/sys/class/infiniband'

# pylint: disable=E0401,W0201,C0301


def network_io_counters(data):
    """This method gets the system wide network IO counters"""

    # Get system wide network IO counters.
    for key, value in data['net_io_counters'].items():
        value.append(getattr(psutil.net_io_counters(pernic=False, nowrap=True), key))

    return data


def ib_io_counters(ib_ports, data):
    """This method gets the IB port counters"""

    # Get Infiniband IO counters. This includes RoCE as well on p3-alaska
    for key, value in data['ib_io_counters'].items():
        cum_value = 0
        for name, port in ib_ports.items():
            counter_path = os.path.join(IB_API_AIR, name, 'ports', port, 'counters', key)
            if os.path.isfile(counter_path):
                content = open(counter_path, 'r').read().rstrip()
                try:
                    # Data port counters indicate octets divided by 4 rather than just octets.
                    counter_value = int(content)
                except ValueError:
                    counter_value = 0
                # Multiply by four to calculate bytes
                cum_value += counter_value * 4
        value.append(cum_value)

    return data
