""""This package contains core modules of SDP performance monitoring toolkit"""

import os
import sys
import shutil
import logging

from perfmon.common.utils.json_wrappers import load_json
from perfmon.common.utils.json_wrappers import write_json

_log = logging.getLogger(__name__)


class MergeFiles(object):
    """This class contains methods to merge individual files from each node into a single file"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config.copy()

    def merge_data_files(self, root, filenames, metric_name):
        """Merge metric meta data files"""

        # Name of the merge JSON file
        merged_filename = os.path.join(self.config['data_dir'], metric_name + ".json")

        # Initialise the JSON file
        merged_file = {'host_names': []}

        # Add metric file from each host to the merged file
        for jsonfile in filenames:
            if jsonfile.startswith(str(self.config['job_id'])):
                content = load_json(os.path.join(root, jsonfile))
                try:
                    host_name = content['host_name']
                    merged_file['host_names'].append(host_name)
                    content.pop('host_name', None)
                    if metric_name in ["cpu_metrics", "perf_metrics"]:
                        merged_file['sampling_frequency'] = content['sampling_frequency']
                        content.pop('sampling_frequency', None)
                    merged_file[host_name] = content
                except KeyError:
                    pass

        # Write the merged file to disk
        write_json(merged_file, merged_filename)

    def merge_multi_json_files(self):
        """Merge json files from all nodes into single one"""

        _log.info("Merging all the output files into one single file")

        # Merge metric data from different hosts to a single file and delete raw data
        for metric in self.config['metrics']:
            raw_data_folder = self.config['temp_path'][metric]
            root, _, filenames = next(os.walk(raw_data_folder))
            self.merge_data_files(root, filenames, metric)

        _log.info("Successfully merged data from all nodes into single file")

        # Delete the raw data files
        raw_data_dir = os.path.dirname(self.config['temp_path']['cpu_metrics'])
        shutil.rmtree(raw_data_dir)

    def remove_ipc_file(self):
        """ "Remove Inter Process Communicator file, if present"""

        if os.path.isfile(self.config['ipc_file']):
            os.remove(self.config['ipc_file'])

    def go(self):
        """Entry point for merging files"""

        _log.info("All nodes finished collecting metrics. Post processing the data files...")
        self.merge_multi_json_files()
        self.remove_ipc_file()
