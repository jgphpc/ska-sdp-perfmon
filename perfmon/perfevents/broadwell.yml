# This file contains several perf groups and event raw codes for broadwell architecture
# More details on perf events and event codes can be found at https://man7.org/linux/man-pages/man1/perf-list.1.html
# This package lists all the perf events on a given processor -> https://sourceforge.net/p/perfmon2/libpfm4/ci/master/tree/
# Most of the stuff here is actually taken from https://github.com/RRZE-HPC/likwid/tree/master/groups
---
# These events are available on almost all processors under the given names. No event codes are really necessary to get these counters
# :u indicates we are only monitoring events at the user level
hardware_events:
  # Hardware specific events
  events:
    - "cycles:u"
    - "instructions:u"
    - "cache-misses:u"
    - "cache-references:u"
    - "branches:u"
    - "branch-misses:u"

software_events:
  # Software specific events
  events:
    - "context-switches:u"
    - "cpu-migrations:u"

# L2 cache bandwidth in MBytes/s
# The bandwidth is computed by the number of cache line loaded from the L2 to the L2 data cache and the writebacks from the L2 data cache to the L2 cache.
L2:
  events:
    # Bandwidth in loading cache lines from L2 to L1 data cache
    - "cpu/event=0x51,umask=0x01,name=L1D_REPLACEMENT/u"
    # Bandwidth in writeback (eviction) from L1 data cache to L2
    - "cpu/event=0xF0,umask=0x10,name=L2_TRANS_L1D_WB/u"
    # Bandwidth in cache lines transferred to the instruction cache.
    - "cpu/event=0x80,umask=0x02,name=L2_ICACHE_MISSES/u"
  # Formula to compute total data transferred in MB. It will be transformed into bandwidth during run time based on the sampling interval time
  formulae:
    derv_total_L2_BW: "1e-06*(L1D_REPLACEMENT+L2_TRANS_L1D_WB+L2_ICACHE_MISSES)*64"

# This group measures the locality of your data accesses with regard to the L2 cache. L2 miss ratio tells you how many of your memory references required a cache line to be loaded from a higher level. While the data cache miss rate might be given by your algorithm you should try to get data cache miss ratio as low as possible by increasing your cache reuse.
L2cache:
  events:
    # L2 transfers all
    - "cpu/event=0xF0,umask=0x80,name=L2_TRANS_ALL/u"
    # L2 missed requests
    - "cpu/event=0x24,umask=0x3F,name=L2_RQSTS_MISS/u"
  formulae:
    derv_total_L2_cache_miss_rate: "L2_RQSTS_MISS/L2_TRANS_ALL"

# The bandwidth is computed by the number of cache line allocated in the L2 and the number of modified cache lines evicted from the L2. This group also output data volume transferred between the L3 and measured cores L2 caches. Note that this bandwidth also includes data transfers due to a write allocate load on a store miss in L2.
L3:
  events:
    - "cpu/event=0xF1,umask=0x07,name=L2_LINES_IN_ALL/u"
    - "cpu/event=0xF0,umask=0x40,name=L2_TRANS_L2_WB/u"
  formulae:
    derv_total_L3_BW: "1e-06*(L2_LINES_IN_ALL+L2_TRANS_L2_WB)*64"

# This group measures the locality of your data accesses with regard to the L3 cache. L3 miss ratio tells you how many of your memory references required a cache line to be loaded from a higher level. While the# data cache miss rate might be given by your algorithm you should try to get data cache miss ratio as low as possible by increasing your cache reuse.
L3cache:
  events:
    # L2 transfers all
    - "cpu/event=0xD1,umask=0x24,name=MEM_LOAD_UOPS_RETIRED_L3_ALL/u"
    # L2 missed requests
    - "cpu/event=0xD1,umask=0x20,name=MEM_LOAD_UOPS_RETIRED_L3_MISS/u"
  formulae:
    derv_total_L3_cache_miss_rate: "MEM_LOAD_UOPS_RETIRED_L3_MISS/MEM_LOAD_UOPS_RETIRED_L3_ALL"

# This group measures Single Precision FLOPS
FLOPS_SP:
  events:
    # Scalar
    - "cpu/event=0xC7,umask=0x02,name=FP_ARITH_INST_RETIRED_SCALAR_SINGLE/u"
    - "cpu/event=0xC7,umask=0x08,name=FP_ARITH_INST_RETIRED_128B_PACKED_SINGLE/u"
    # AVX Single Precision
    - "cpu/event=0xC7,umask=0x20,name=FP_ARITH_INST_RETIRED_256B_PACKED_SINGLE/u"
  formulae:
    derv_total_SP_FLOPS: "1e-06*(4*FP_ARITH_INST_RETIRED_128B_PACKED_SINGLE+FP_ARITH_INST_RETIRED_SCALAR_SINGLE+8*FP_ARITH_INST_RETIRED_256B_PACKED_SINGLE)"

# This group measures Double Precision FLOPS
FLOPS_DP:
  events:
    # Scalar
    - "cpu/event=0xC7,umask=0x01,name=FP_ARITH_INST_RETIRED_SCALAR_DOUBLE/u"
    - "cpu/event=0xC7,umask=0x04,name=FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE/u"
    # AVX Double Precision
    - "cpu/event=0xC7,umask=0x10,name=FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE/u"
  formulae:
    derv_total_DP_FLOPS: "1e-06*(2*FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE+FP_ARITH_INST_RETIRED_SCALAR_DOUBLE+4*FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE)"

# Top down cycle allocation. This performance group measures cycles to determine percentage of time spent in front end, back end, retiring and speculation. These metrics are published and verified by Intel. Further information: Webpage describing Top-Down Method and its usage in Intel vTune: https://software.intel.com/en-us/vtune-amplifier-help-tuning-applications-using-a-top-down-microarchitecture-analysis-method
# Paper by Yasin Ahmad:
# https://sites.google.com/site/analysismethods/yasin-pubs/TopDown-Yasin-ISPASS14.pdf?attredirects=0
# Slides by Yasin Ahmad:
# http://www.cs.technion.ac.il/~erangi/TMA_using_Linux_perf__Ahmad_Yasin.pdf
# The performance group was originally published here:
# http://perf.mvermeulen.com/2018/04/14/top-down-performance-counter-analysis-part-1-likwid/
# Todo
