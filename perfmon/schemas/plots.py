"""This is schema for plots"""


# Bytes to MiB conversion factor
BYTES2MiB = 1048576

# Micro Joules to Joules conversion
UJ2J = 1000000

base_metric_labels = {
    'System wide CPU Percent [%]': {
        'name': 'CPU Load',
        'cat': 'CPU',
        'units': '%',
        'comb': 'Average',
        'convert_to_rate': False,
        'unit_conversion': 1,
        'log_scale': False,
        'size': 8,
    },
    'USS memory [bytes]': {
        'name': 'Memory',
        'cat': 'Memory',
        'units': 'MiB',
        'comb': 'Average',
        'convert_to_rate': False,
        'unit_conversion': BYTES2MiB,
        'log_scale': False,
        'size': 8,
    },
    'Network recv data [bytes]': {
        'name': 'Input Traffic',
        'cat': 'Network IO',
        'units': 'MiB/s',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': BYTES2MiB,
        'log_scale': False,
        'size': 7,
    },
    'Network sent data [bytes]': {
        'name': 'Output Traffic',
        'cat': 'Network IO',
        'units': 'MiB/s',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': BYTES2MiB,
        'log_scale': False,
        'size': 7,
    },
    'Network recv packets': {
        'name': 'Packets Input',
        'cat': 'Network IO',
        'units': '#pkts/s',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': 1,
        'log_scale': True,
        'size': 7,
    },
    'Network sent packets': {
        'name': 'Packets Output',
        'cat': 'Network IO',
        'units': '#pkts/s',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': 1,
        'log_scale': True,
        'size': 7,
    },
}

ib_metric_labels = {
    'IB recv data [bytes]': {
        'name': 'Interconnect Input Traffic',
        'cat': 'Network IO',
        'units': 'MiB/s',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': BYTES2MiB,
        'log_scale': False,
        'size': 7,
    },
    'IB xmit data [bytes]': {
        'name': 'Interconnect Output Traffic',
        'cat': 'Network IO',
        'units': 'MiB/s',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': BYTES2MiB,
        'log_scale': False,
        'size': 7,
    },
    'IB recv packets': {
        'name': 'Interconnect Packets Input',
        'cat': 'Network IO',
        'units': '#pkts/s',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': 1,
        'log_scale': True,
        'size': 7,
    },
    'IB xmit packets': {
        'name': 'Interconnect Packets Output',
        'cat': 'Network IO',
        'units': '#pkts/s',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': 1,
        'log_scale': True,
        'size': 7,
    },
}

mem_bw_metric_label = {
    'Memory (read) bandwidth [MiB/s]': {
        'name': 'Memory Bandwidth (Read)',
        'cat': 'Memory',
        'units': 'MiB/s',
        'comb': 'Average',
        'convert_to_rate': False,
        'unit_conversion': BYTES2MiB,
        'log_scale': False,
        'size': 8,
    },
}

engy_package_metric_label = {
    'RAPL package': {
        'name': 'RAPL Package Power',
        'cat': 'Energy',
        'units': 'W',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': UJ2J,
        'log_scale': False,
        'size': 7,
    },
}

engy_dram_metric_label = {
    'RAPL dram': {
        'name': 'RAPL DRAM Power',
        'cat': 'Energy',
        'units': 'W',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': UJ2J,
        'log_scale': False,
        'size': 7,
    },
}

engy_core_metric_label = {
    'RAPL core': {
        'name': 'RAPL Core Power',
        'cat': 'Energy',
        'units': 'W',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': UJ2J,
        'log_scale': False,
        'size': 7,
    },
}

engy_uncore_metric_label = {
    'RAPL uncore': {
        'name': 'RAPL Uncore Power',
        'cat': 'Energy',
        'units': 'W',
        'comb': 'Total',
        'convert_to_rate': True,
        'unit_conversion': UJ2J,
        'log_scale': False,
        'size': 7,
    },
}
