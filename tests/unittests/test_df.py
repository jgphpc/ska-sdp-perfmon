#!/usr/bin/env python3
""" This script does the unit tests for the dataframe related classes"""

import os
import sys
import pathlib
import unittest
import shutil
import yaml
import pandas
from pandas._testing import assert_frame_equal


from perfmon.common.df import CreateDataFrame
from perfmon.exceptions import MetricGroupNotImplementedError


class TestSuite(unittest.TestCase):
    """Test Dataframe class"""

    def setup(self):
        """Load CI configuration"""

        project_root = pathlib.Path(__file__).parent.parent.parent

        with open(os.path.join(
                project_root,
                'tests/unittests/data/configs/test-0.host.name_run_config.yml'),
                'r') as cfg_file:
            self.config = yaml.full_load(cfg_file)

        self.config['data_dir'] = os.path.join(
            project_root, 'tests/unittests/data/metrics'
        )

        self.config['perf_event_file'] = os.path.join(
            project_root, 'tests/unittests/data/perf_event_list'
        )

        # Result file read from csv
        self.df_cpu = pandas.read_csv(
            os.path.join(
                project_root, 'tests/unittests/data/result/cpu_metrics.csv'
            )
        )

        # Result file read from csv
        self.df_perf = pandas.read_csv(
            os.path.join(
                project_root, 'tests/unittests/data/result/perf_metrics.csv'
            )
        )

    def test_cpu_metrics(self):
        """Test the cpu metrics dataframe"""

        self.setup()
        create_df = CreateDataFrame('cpu_metrics', self.config)
        df_check = create_df.go()

        assert_frame_equal(df_check, self.df_cpu)

        # self.clean_up(test_config)

    def test_perf_metrics(self):
        """Test the perf metrics dataframe"""

        self.setup()
        create_df = CreateDataFrame('perf_metrics', self.config)
        df_check = create_df.go()

        assert_frame_equal(df_check, self.df_perf)

        # self.clean_up(test_config)

    def test_metric_not_found(self):

        self.setup()
        create_df = CreateDataFrame('dummy_metric', self.config)

        with self.assertRaises(MetricGroupNotImplementedError):
            create_df.go()


if __name__ == '__main__':
    unittest.main(verbosity=2)
