#!/usr/bin/env python3
""" This script does the unit tests for the dataframe related classes"""

import os
import unittest
import pandas


from perfmon.common.export import ExportData


class TestSuite(unittest.TestCase):
    """Test export data class"""

    def setup(self):
        """Load CI configuration"""

        # Set GCP service account key file env variable
        self.config = {
            'save_dir': os.getcwd(),
            'metrics': ['cpu_metrics', 'perf_metrics']
        }

        df = pandas.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]},
                                index=['a', 'b', 'c'])

        self.df_dict = {}
        for metric in self.config['metrics']:
            self.df_dict[metric] = df

    def test_export_data_csv(self):
        """Test export data class"""

        self.setup()
        self.config = {
            **self.config,
            'export': ['csv'],
        }

        export_data = ExportData(config=self.config,
                                 df_dict=self.df_dict)
        export_data.go()

        for metric in self.config['metrics']:
            file_name = os.path.join(self.config['save_dir'], '.'.join([metric, 'csv']))
            assert os.path.isfile(file_name)
            # Cleanup
            os.remove(file_name)

    def test_export_data_hdf5(self):
        """Test export data class"""

        self.setup()
        self.config = {
            **self.config,
            'export': ['hdf5'],
        }

        export_data = ExportData(config=self.config,
                                 df_dict=self.df_dict)
        export_data.go()

        for metric in self.config['metrics']:
            file_name = os.path.join(self.config['save_dir'], '.'.join([metric, 'h5']))
            assert os.path.isfile(file_name)
            # Cleanup
            os.remove(file_name)

    def test_export_data_feather(self):
        """Test export data class"""

        self.setup()
        self.config = {
            **self.config,
            'export': ['feather'],
        }

        export_data = ExportData(config=self.config,
                                 df_dict=self.df_dict)
        export_data.go()

        for metric in self.config['metrics']:
            file_name = os.path.join(self.config['save_dir'], '.'.join([metric, 'feather']))
            assert os.path.isfile(file_name)
            # Cleanup
            os.remove(file_name)

    def test_export_data_parquet(self):
        """Test export data class"""

        self.setup()
        self.config = {
            **self.config,
            'export': ['parquet'],
        }

        export_data = ExportData(config=self.config,
                                 df_dict=self.df_dict)
        export_data.go()

        for metric in self.config['metrics']:
            file_name = os.path.join(self.config['save_dir'], '.'.join([metric, 'parquet']))
            assert os.path.isfile(file_name)
            # Cleanup
            os.remove(file_name)

    def test_export_data_pickle(self):
        """Test export data class"""

        self.setup()
        self.config = {
            **self.config,
            'export': ['pickle'],
        }

        export_data = ExportData(config=self.config,
                                 df_dict=self.df_dict)
        export_data.go()

        for metric in self.config['metrics']:
            file_name = os.path.join(self.config['save_dir'], '.'.join([metric, 'pkl']))
            assert os.path.isfile(file_name)
            # Cleanup
            os.remove(file_name)

    def test_export_data_orc(self):
        """Test export data class"""

        self.setup()
        self.config = {
            **self.config,
            'export': ['orc'],
        }

        export_data = ExportData(config=self.config,
                                 df_dict=self.df_dict)
        export_data.go()

        for metric in self.config['metrics']:
            file_name = os.path.join(self.config['save_dir'], '.'.join([metric, 'orc']))
            assert os.path.isfile(file_name)
            # Cleanup
            os.remove(file_name)


if __name__ == '__main__':
    unittest.main(verbosity=2)
